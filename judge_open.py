# -*- coding:utf-8 -*-
try:
    file_name = input("请输入需要打开的文件名：")#文件名包含后缀如：README.txt.单独输入名字会出错
    f = open(file_name)#maybe OSError
    try:
    
##        sum ='1'+1#TypeError
##        int('abc')#ValueError
        print("文件的内容是：")
        for each_line in f:
            print(each_line)
    except OSError as reason:
        print("出错T_T\n错误的原因是："+str(reason))
    except (TypeError,ValueError) as reason:#可以同时捕获多种异常
        print("出错啦\n错误的原因是："+str(reason))
    finally:
        f.close()#关闭文件 此时f.closed为false，说明文件被打开了。（我没有用到closed属性，因为函数到达这里，文件肯定已经打开了）
except OSError as reason:
    print("文件打开出错T_T\n错误的原因是："+str(reason))


raise Exception("人为的抛出错误")
