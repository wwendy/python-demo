def cheeseshop(kind , *arguments , **keywords):
    print '--do you have any' , kind , '?'
    print "--I'm sorry , we're all out of " , kind
    for arg in arguments:
        print arg
    print '-' * 40
    keys = keywords.keys()
    keys.sort()
    for kw in keys:
        print kw , ":" , keywords[kw]
cheeseshop('fruit' ,
           "  I't very runny , sir" ,
           "  I't really very very sunny , sir" ,
           "  I,m sorry , welcom next time",
           client = "phil",
           shopkeeper = "wendy" ,
           sketch = "cheese shop sketch")
