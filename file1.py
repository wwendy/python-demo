# coding = utf-8
# -*- coding:cp936 -*-
import os , shutil
import datetime

DIR = 'D:\\'
FILENAME = 'BeginMan'
PATH = 'D:\\%s' % FILENAME
CONTENT = """
            （1）：检查下磁盘中(D)是否有文件"BeginMan"的文件夹，如果存在则新建"SuperMan",\n
            在该目录下创建一个名为"readme.txt"的文本文件，写入数据，然后输出文件内容\n
            （2）：如果文件夹BeginMan存在，则遍历该目录，并输入目录文件\n，
            如果该目录下存在文件，且如果有文件名为b.png，则重命名为boy.png，移到D盘根目录下\n
            (3)：删除BeginMan目录下所有文件，只保留子目录
            ==============================================\n
            时间：%s\n
            作者：%s\n
            """ %(datetime.datetime.now(),FILENAME)
if FILENAME in os.listdir(DIR):
        print u'已经有同名文件：%s' %FILENAME
        for obj in os.listdir(PATH):
            print  u'%s\\%s\n' %(PATH,obj)
            try:
                if os.path.isfile(u'%s\\%s' %(PATH,obj)):
                    if obj == 'b.png':  #如果含有文件为b.png，则重命名为boy.png,移动到该目录上级
                        os.rename(u'%s\\%s' %(PATH,obj),u'%s\\%s' %(PATH,'boy.png'))
                        shutil.move(u'%s\\%s' %(PATH,'boy.png'), 'D:\\')
                    os.remove(u'%s\\%s' %(PATH,obj))#删除文件,只保留子目录
            except Exception,e:
                print 'The error:%s' %e
        FILENAME = 'SuperMan'
        os.mkdir('D:\\%s' %FILENAME)                    #创建目录
        f = open('D:\\%s\\readme.txt'%FILENAME,'w' )    #在该目录下创建文件
        f.write(CONTENT)                                #写入数据
        f.close()
        f = open('D:\\%s\\readme.txt'%FILENAME,'r' )
        print [lines.strip() for lines in f]            #输出
